public class Car {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public int vin;
    public int days;
    public boolean ifOnTruckSpace;

    public Car(int vin, int days, boolean ifOnTruckSpace) {
        this.vin = vin;
        this.days = days;
        this.ifOnTruckSpace = ifOnTruckSpace;
    }


    public int getVin() {
        return vin;
    }

    public void setVin(int vin) {
        this.vin = vin;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    @Override
    public String toString() {
        if (vin == -1) {
            return "Вакантное место";
        }
        if (ifOnTruckSpace) {
            return "Автомобиль с VIN номером " + ANSI_BLUE + +vin + ANSI_RESET +
                    " будет здесь стоять на месте для грузовиков еще вот столько дней: " + ANSI_BLUE + days + ANSI_RESET;
        } else {
            return "Автомобиль с VIN номером " + ANSI_BLUE + +vin + ANSI_RESET +
                    " будет здесь стоять на месте для легковушек еще вот столько дней: " + ANSI_BLUE + days + ANSI_RESET;


        }

    }
}


