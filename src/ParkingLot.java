import java.util.Random;
import java.util.Scanner;

public class ParkingLot {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_BLUE = "\u001B[34m";
    int freeSpaces = Manager.slotsCar;
    int freeSpacesTrucks = Manager.slotsTruck;
    int currentDay = 1;
    int howManyCarsWillCome;
    int howManyTrucksWillCome;
    int minDays = 10;
    boolean noTruckSpaces;
    Car[] cars;
    Car[] trucks;
    String phrase;


    public ParkingLot() {
    }

    public void startingPoint() {
        freeSpaces = Manager.slotsCar;
        freeSpacesTrucks = Manager.slotsTruck;
        cars = new Car[freeSpaces];
        trucks = new Car[freeSpacesTrucks];
        for (int i = 0; i < freeSpaces; i++) {
            cars[i] = new Car(-1, -1, false);
        }
        for (int i = 0; i < freeSpacesTrucks; i++) {
            trucks[i] = new Car(-2, -1, false);
        }
        newCars();
    }

    public void newCars() {
        howManyCarsWillCome = 0;
        daysDecrease();
        System.out.println("День " + currentDay);
        currentDay++;
        Random random = new Random();

        if (freeSpaces == 1 | freeSpaces == 2) {
            howManyCarsWillCome = 1;

        } else if (freeSpaces != 0) {
            howManyCarsWillCome = random.nextInt(freeSpaces / 3) + 1;
        }
        if (freeSpaces != 0) {
            for (int i = 0; i < howManyCarsWillCome; i++) {
                while (cars[i].getVin() != -1) {
                    i++;
                }
                int vin = random.nextInt(9000) + 1000;
                int days = random.nextInt(10) + 1;
                cars[i] = new Car(vin, days, false);
                System.out.println("Приехала новая легковушка с VIN " + ANSI_BLUE + vin + ANSI_RESET + ". Стоять ей ещё вот " +
                        "столько дней: " + ANSI_BLUE + days + ANSI_RESET);
                cars[i].setDays(cars[i].getDays());
                freeSpaces--;
            }
        } else {
            System.out.println(ANSI_RED + "Нет свободных парковочных мест для легковых!" + ANSI_RESET);
            howLongToWait(Manager.slotsCar - freeSpaces, cars, false);
        }
        newTrucks();
    }

    public void newTrucks() {
        Random random = new Random();
        noTruckSpaces = false;
        howManyTrucksWillCome = 0;

        if (freeSpacesTrucks >= 1) {
            if (freeSpacesTrucks >= 3) {
                howManyTrucksWillCome = random.nextInt(freeSpacesTrucks / 3) + 1;
            } else {
                howManyTrucksWillCome = 1;
            }
        } else if (freeSpaces >= 2) {
            if (freeSpaces <= 3) {
                howManyTrucksWillCome = 1;
            } else {
                howManyTrucksWillCome = random.nextInt(freeSpaces / 3) + 1;
            }
            noTruckSpaces = true;
        }

        if (howManyTrucksWillCome != 0) {
            for (int i = 0; i < howManyTrucksWillCome; i++) {
                int vin = random.nextInt(90000) + 10000;
                int days = random.nextInt(10) + 1;
                if (noTruckSpaces) {
                    truckOnCarSpace(vin, days);

                } else {
                    while (trucks[i].getVin() != -2) {
                        i++;
                    }
                    trucks[i] = new Car(vin, days, true);
                    trucks[i].setDays(trucks[i].getDays());
                    freeSpacesTrucks--;
                }
                System.out.println("Приехал новый грузовик с VIN " + ANSI_BLUE + vin + ANSI_RESET + ". " +
                        "Стоять ему ещё вот столько дней: " + ANSI_BLUE + days + ANSI_RESET);
            }
        } else {
            System.out.println(ANSI_RED + "Нет свободных парковочных мест для грузовых!" + ANSI_RESET);
            howLongToWait(Manager.slotsTruck - freeSpacesTrucks, trucks, true);
        }
        endOfTheDay();
    }

    public void truckOnCarSpace(int vin, int days) {
        for (int i = 0; i < Manager.slotsCar - 1; i++) {
            if (cars[i].getVin() == -1 & cars[i + 1].getVin() == -1) {
                freeSpaces = freeSpaces - 2;
                cars[i] = new Car(vin, days, false);
                cars[i].setDays(cars[i].getDays());
                cars[i + 1] = new Car(vin, days, false);
                cars[i + 1].setDays(cars[i + 1].getDays());
                break;
            }
        }
    }

    public void clearTheParking() {
        System.out.println("Все машины отправлены в утиль!\n");
        startingPoint();
    }

    public void endOfTheDay() {
        System.out.println();
        System.out.println("Введите 1 для перехода на следующий день\n" +
                "Введите 2, чтобы узнать, сколько машин на данный момент стоит в паркинге\n" +
                "Введите 3, чтобы отобразить список машин\n" +
                "Введите 4, чтобы очистить парковку\n" +
                "Введите 5, чтобы отобразить справку по VIN номерам\n");
        Scanner scan = new Scanner(System.in);
        phrase = scan.nextLine();
        if (phrase.equalsIgnoreCase("1")) {
            newCars();
        }
        if (phrase.equalsIgnoreCase("2")) {
            howManyCars();
        }
        if (phrase.equalsIgnoreCase("3")) {
            listOfCars();
        }
        if (phrase.equalsIgnoreCase("4")) {
            clearTheParking();
        }
        if (phrase.equalsIgnoreCase("5")) {
            System.out.println("У каждого автомобиля есть свой уникальный " + ANSI_BLUE + "VIN" + ANSI_RESET + " номер." +
                    " У легковых авто он четырёхзначный, \n" +
                    "у грузовых же - пятизначный.");
            endOfTheDay();
        } else {
            System.out.println("Ладно, неважно. Начинаю новый день.");
            newCars();
        }
    }

    public void daysDecrease() {
        for (int i = 0; i < Manager.slotsCar; i++) {
            if (cars[i].getDays() == 1 & cars[i].getVin() <= 9999 & cars[i].getVin() >= 1000) {
                cars[i].setVin(-1);
                freeSpaces++;

            } else if (cars[i].getDays() == 1 & cars[i].getVin() > 9999) {
                cars[i].setVin(-1);
                cars[i + 1].setVin(-1);
                freeSpaces = freeSpaces + 2;
            }
            cars[i].setDays(cars[i].getDays() - 1);
        }
        for (int i = 0; i < Manager.slotsTruck; i++) {
            if (trucks[i].getDays() == 1) {
                trucks[i].setVin(-2);
                freeSpacesTrucks++;
            }

            trucks[i].setDays(trucks[i].getDays() - 1);
        }
    }

    public void listOfCars() {
        for (int i = 0; i < Manager.slotsCar - freeSpaces; i++) {
            if (cars[i].getVin() > 9999) {
                truckOnCarSpaceList(i);
                i++;
            } else {
                if (cars[i].getVin() == -1) {
                    i++;
                }
                System.out.println(cars[i].toString());
            }
        }
        for (int i = 0; i < Manager.slotsTruck - freeSpacesTrucks; i++) {
            while (trucks[i].getVin() == -2) {
                i++;
            }
            System.out.println(trucks[i].toString());
        }
        endOfTheDay();
    }

    public void truckOnCarSpaceList(int i) {
        while (cars[i].getVin() == -1 | cars[i + 1].getVin() == -1) {
            i++;
        }
        System.out.println(cars[i].toString());
    }


    public void howLongToWait(int howManyCarsThere, Car[] anyCars, boolean ifTruck) {
        minDays = 10;
        for (int i = 0; i < howManyCarsThere; i++) {
            if (!ifTruck) {
                if (anyCars[i].getVin() <= 9999) {
                    if (anyCars[i].getDays() < minDays) {
                        minDays = anyCars[i].getDays();
                    }
                }
            } else {
                if (anyCars[i].getDays() < minDays) {
                    minDays = anyCars[i].getDays();
                }
            }
        }
        System.out.println("Ближайшее освободится через вот столько дней: " + minDays);
    }

    public void howManyCars() {
        int howManyCars = 0;
        int howManyTrucks = 0;
        for (int i = 0; i < Manager.slotsCar; i++) {
            if (cars[i].getVin() > 999 & cars[i].getVin() < 10000) {
                howManyCars++;
            } else if (cars[i].getVin() >= 10000) {
                howManyTrucks++;
            }
        }
        howManyTrucks = howManyTrucks / 2;
        for (int i = 0; i < Manager.slotsTruck; i++) {
            if (trucks[i].getVin() > 9999) {
                howManyTrucks++;
            }
        }
        System.out.println("На парковке стоит " + howManyCars + " легковых авто и " + howManyTrucks + " грузовик(-ов)\n");
        endOfTheDay();
    }
}