import java.util.Scanner;

public class Manager {
    public static int slotsCar;
    public static int slotsTruck;
    static ParkingLot theParking = new ParkingLot();


    public static void main(String[] args) {
        initial();
    }

    public static void initial() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Итак, вы создаёте паркинг. Сколько там должно быть мест для легковых авто?");
        slotsCar = sc.nextShort();
        System.out.println("А сколько для грузовых?");
        slotsTruck = sc.nextShort();
        if (slotsCar < 4 | slotsCar > 100 | slotsTruck > 100) {
            System.out.println("Ваш паркинг должен иметь хотя бы 4 парковочных места, но их так же не должно быть больше 100");
            initial();
        }
        theParking.startingPoint();
    }


}
